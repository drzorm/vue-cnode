// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  "plugins": {
    // to edit target browsers: use "browserslist" field in package.json
    "autoprefixer": {
      browsers: ["last 10 Chrome versions", "Safari >= 6"]
    },
    "postcss-px2rem": {
      remUnit: 64
    }
  }
}
