import fetch from '../utils/fetch'
import toast from './toast/toast'

export default {
  install(Vue, options) {
    // 添加全局方法或属性
    Vue.myGlobalMethod = function () {}
    // 添加全局资源
    Vue.directive('my-directive', {
      bind(el, binding, vnode, oldVnode) {}
    })
    // 注入组件
    Vue.mixin({
      data: function () {
        return {
          userinfo: JSON.parse(localStorage.getItem('USERINFO') || '{}'),
          accesstoken: localStorage.getItem('ACCESSTOKEN')
        }
      },
      computed: {
      }
    })
    // 添加实例方法
    // fetch axios封装
    Vue.prototype.$http = fetch
    // toast轻提示
    Vue.prototype.$toast = toast
  }
}
