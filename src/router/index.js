import Vue from 'vue'
import Router from 'vue-router'
import topicTypeList from '@/utils/topicTypeList'
import list from '@/views/list'
import topic from '@/views/topic'
import message from '@/views/message'
import setting from '@/views/setting'
import about from '@/views/about'
import login from '@/views/login'
import user from '@/views/user'

Vue.use(Router)

let topicListRoute = topicTypeList.map(n => {
  return {
    path: `/list/:tab(${n.tab})`,
    name: n.tab,
    component: list,
    meta: {
      title: n.name
    },
    children : [{
      path: ':id',
      component: topic,
      meta: {
        title: '话题'
      }
    }]
  }
})

let baseRouter = [{
  path: '/topic/:id',
  name: 'topic',
  component: topic,
  meta: {
    title: '话题'
  }
}, {
  path: '/message',
  name: 'message',
  component: message,
  meta: {
    title: '消息'
  }
}, {
  path: '/setting',
  name: 'setting',
  component: setting,
  meta: {
    title: '设置'
  }
}, {
  path: '/about',
  name: 'about',
  component: about,
  meta: {
    title: '关于'
  }
}, {
  path: '/login',
  name: 'login',
  component: login,
  meta: {
    title: '登录'
  }
}, {
  path: '/user',
  name: 'center',
  component: user,
  meta: {
    title: '个人中心'
  }
}, {
  path: '/user/:id',
  name: 'user',
  component: user,
  meta: {
    title: '用户中心'
  }
}, {
  path: '*',
  redirect: '/list/all'
}]

export default new Router({
  routes: [...topicListRoute, ...baseRouter]
})
