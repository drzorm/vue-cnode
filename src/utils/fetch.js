import {
  VERSION,
  APIURL
} from '@/config'
// https://github.com/stefanpenner/es6-promise
import Promise from 'es6-promise'
Promise.polyfill()
// https://github.com/mzabriskie/axios
import axios from 'axios'

let urlRegx = /^https?:\/\/(([a-zA-Z0-9_-])+(\.)?)*(:\d+)?(\/((\.)?(\?)?=?&?[a-zA-Z0-9_-](\?)?)*)*$/i

const fetch = axios.create({
  timeout: 10000
})

// request interceptor
fetch.interceptors.request.use(function (config) {
  config.url = urlRegx.test(config.url) ? config.url : `${APIURL}${config.url}`
  console.info(`request ${config.method} ${config.url}\ndata:`, config.data || {}, `\nparams:`, config.params || {})
  return config
}, function (error) {
  return Promise.reject(error)
})

// response interceptor
fetch.interceptors.response.use(function (response) {
  console.info(`*response* ${response.config.url}\n`, response.data)
  return response.data
}, function (error) {
  if(error.response){
    return error.response.data
  }else{
    return Promise.reject(error)
  }
})

export default fetch
