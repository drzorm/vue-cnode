import fetch from '@/utils/fetch'

/**
 * [count 获取未读消息数]
 * @param  {[String]} accesstoken [description]
 * @return {[Promise]}             [description]
 */
const count = (accesstoken) => {
  return fetch({
    method: 'GET',
    params: {
      accesstoken: accesstoken
    },
    url: '/message/count'
  })
}

/**
 * [messages 获取已读和未读消息]
 * @param  {[String]} accesstoken [description]
 * @return {[Promise]}             [description]
 */
const messages = (accesstoken) => {
  return fetch({
    method: 'GET',
    params: {
      accesstoken: accesstoken,
      mdrender: true
    },
    url: '/messages'
  })
}

/**
 * [markAll 标记全部已读]
 * @param  {[String]} accesstoken [description]
 * @return {[Promise]}             [description]
 */
const markAll = (accesstoken) => {
  return fetch({
    method: 'POST',
    data: {
      accesstoken: accesstoken
    },
    url: '/message/mark_all'
  })
}

export default { count, messages, markAll }