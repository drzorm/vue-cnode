import fetch from '@/utils/fetch'

/**
 * [replies 新建评论]
 * @param  {[String]} options.accesstoken [description]
 * @param  {[String]} options.topic_id    [description]
 * @param  {[String]} options.content     [description]
 * @param  {String} options.reply_id    }            [description]
 * @return {[Promise]}                     [description]
 */
const replies = ({ accesstoken, topic_id, content, reply_id } = {}) => {
  return fetch({
    method: 'POST',
    data: {
      accesstoken: accesstoken,
      reply_id: reply_id,
      content: content
    },
    url: `/topic/${topic_id}/replies`
  })
}

/**
 * [ups 为评论点赞]
 * @param  {[String]} options.accesstoken [description]
 * @param  {String} options.reply_id    }            [description]
 * @return {[Promise]}                     [description]
 */
const ups = ({ accesstoken, reply_id } = {}) => {
  return fetch({
    method: 'POST',
    data: {
      accesstoken: accesstoken
    },
    url: `/reply/${reply_id}/ups`
  })
}

export default { replies, ups }