import fetch from '@/utils/fetch'

/**
 * [addTopic 收藏主题]
 * @param {[String]} options.accesstoken [description]
 * @param {String} options.topic_id               [description]
 * @return {[Promise]}           [description]
 */
const collect = ({ accesstoken, topic_id } = {}) => {
  return fetch({
    method: 'POST',
    data: {
      accesstoken: accesstoken,
      title: title,
      tab: tab,
      content: content
    },
    url: '/topic_collect/collect'
  })
}

/**
 * [addTopic 取消主题]
 * @param {[type]} String.accesstoken [description]
 * @param {String} options.topic_id    }            [description]
 * @return {[Promise]}           [description]
 */
const deCollect = ({ accesstoken, topic_id } = {}) => {
  return fetch({
    method: 'POST',
    data: {
      accesstoken: accesstoken,
      title: title,
      tab: tab,
      content: content
    },
    url: '/topic_collect/de_collect'
  })
}

/**
 * [userCollect 用户所收藏的主题]
 * @param  {[String]} loginname [用户名]
 * @return {[Promise]}           [description]
 */
const userCollect = (loginname) => {
  return fetch({
    method: 'GET',
    url: `/topic_collect/${loginname}`
  })
}

export default { collect, deCollect, userCollect }