import fetch from '@/utils/fetch'

/**
 * @description                       [getTopics 主题首页]
 * @param  {Number} options.page      [页数]
 * @param  {Number} options.limit     [每一页的主题数量]
 * @param  {String} options.tab       [主题分类 目前有 all ask share job good]
 * @param  {String} options.mdrender  [String 当为 false 时，不渲染。默认为 true，渲染出现的所有 markdown 格式文本]
 * @return {Object}                   [主题数据]
 */
const getTopics = ({ page = 1, limit = 20, tab = 'all', mdrender = true } = {}) => {
  return fetch({
    method: 'GET',
    params: {
      page: page,
      limit: limit,
      tab: tab,
      mdrender: mdrender,
    },
    url: '/topics'
  })
}

/**
 * [getTopic 主题详情]
 * @param  {String} id [主题id]
 * @param  {String} accesstoken [当需要知道一个主题是否被特定用户收藏以及对应评论是否被特定用户点赞时，才需要带此参数。会影响返回值中的 is_collect 以及 replies 列表中的 is_uped 值]
 * @param  {String} options.mdrender  [当为 false 时，不渲染。默认为 true，渲染出现的所有 markdown 格式文本]
 * @return {Promise}    [description]
 */
const getTopic = ({ id, mdrender = true } = {}) => {
  return fetch({
    method: 'GET',
    params: {
      mdrender: mdrender
    },
    url: `/topic/${id}`
  })
}

/**
 * [postTopics 新建主题]
 * @param  {String} options.accesstoken [description]
 * @param  {String} options.title       [description]
 * @param  {String} options.tab         [description]
 * @param  {Object} options.content                 [description]
 * @return {Promise}                     [description]
 */
const addTopics = ({ accesstoken, title, tab, content } = {}) => {
  return fetch({
    method: 'POST',
    data: {
      accesstoken: accesstoken,
      title: title,
      tab: tab,
      content: content
    },
    url: '/topics'
  })
}

/**
 * [updateTopics 编辑主题]
 * @param  {String} options.accesstoken [description]
 * @param  {String} options.topic_id    [description]
 * @param  {String} options.title       [description]
 * @param  {String} options.tab         [description]
 * @param  {Object} options.content                 [description]
 * @return {Promise}                     [description]
 */
const updateTopics = ({ accesstoken, topic_id, title, tab, content } = {}) => {
  return fetch({
    method: 'POST',
    data: {
      accesstoken: accesstoken,
      title: title,
      tab: tab,
      content: content
    },
    url: '/topics/update'
  })
}

export default { getTopics, getTopic, addTopics, updateTopics }