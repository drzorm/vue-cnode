import fetch from '@/utils/fetch'

/**
 * [detail description]
 * @param  {[String]} loginname [description]
 * @return {[Promise]}           [description]
 */
const detail = (loginname) => {
  return fetch({
    method: 'GET',
    url: `/user/${loginname}`
  })
}

/**
 * [auth 验证accessToken的正确性]
 * @param  {[String]} accesstoken [用户的 accessToken]
 * @return {[Promise]}             [description]
 */
const auth = (accesstoken) => {
  return fetch({
    method: 'POST',
    data: {
      accesstoken: accesstoken
    },
    url: '/accesstoken'
  })
}


export default { detail, auth }